//
//  ViewController.m
//  ScrollingDemo
//
//  Created by James Cash on 20-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong,nonatomic) UIImageView *airpodsImageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setUpForAirpodsImage];
    [self setUpForiPhoneImages];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - iPhone paging

- (void)setUpForiPhoneImages
{
    NSArray<UIImage*>* images = @[[UIImage imageNamed:@"black"],
                                  [UIImage imageNamed:@"jetblack"],
                                  [UIImage imageNamed:@"gold"],
                                  [UIImage imageNamed:@"rosegold"],
                                  [UIImage imageNamed:@"silver"]];

    CGRect imageFrame = self.scrollView.bounds;
    for (UIImage *phoneImage in images) {
        UIImageView *imgView = [[UIImageView alloc] initWithImage:phoneImage];
        imgView.frame = imageFrame;
        // set the content mode so the images don't get distorted
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        [self.scrollView addSubview:imgView];
        // Move the y coordinate of the frame for the next view down by the height of the image view
        imageFrame = CGRectOffset(imageFrame, 0, imageFrame.size.height);
    }
    // set the content size of the scrollview to contain all the images
    self.scrollView.contentSize = CGSizeMake(imageFrame.size.width,
                                             imageFrame.size.height * images.count);
    // makes the scrollview snap to multiples of its bounds
    self.scrollView.pagingEnabled = YES;
}

#pragma mark - Airpods Zooming
- (void)setUpForAirpodsImage
{
    // first, load the image
    self.airpodsImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"airpods"]];
    // add the imageview as a subview of the scrollview
    [self.scrollView addSubview:self.airpodsImageView];
    // tell the scrollview how big the area we want to scroll over is
    self.scrollView.contentSize = self.airpodsImageView.frame.size;
    // for zooming to work, we need to set a delegate for the scroll view
    self.scrollView.delegate = self;
    // and set how much we want to allow zooming
    self.scrollView.minimumZoomScale = 0.5;
    self.scrollView.maximumZoomScale = 3;
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    // [ScrollView]
    //   -> [V1]
    //   -> [V2]
    // But we want to zoom on v1 & v2 simultaneously
    // [ScrollView]
    //   -> [V0]
    //     -> [V1]
    //     -> [V2]
    // then return v0
    return self.airpodsImageView;
}

@end
